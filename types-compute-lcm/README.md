# Installation
> `npm install --save @types/compute-lcm`

# Summary
This package contains type definitions for compute-lcm (https://github.com/compute-io/lcm#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/compute-lcm.
## [index.d.ts](https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/compute-lcm/index.d.ts)
````ts
// Type definitions for compute-lcm 1.1
// Project: https://github.com/compute-io/lcm#readme
// Definitions by: James Holman <https://github.com/jayy-lmao>
// Definitions: https://github.com/DefinitelyTyped/DefinitelyTyped

type Accessor<T> = (item: T, idx: number) => number | null;

declare function lcm(a: number, b: number): number | null;
declare function lcm(a: ReadonlyArray<number>): number | null;
// Function to extract number from an element from the data array
declare function lcm<T>(dataArr: ReadonlyArray<T>, a: Accessor<T>): number | null;

//  Original package uses module.exports
export = lcm;

````

### Additional Details
 * Last updated: Sat, 25 Sep 2021 01:01:25 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [James Holman](https://github.com/jayy-lmao).
