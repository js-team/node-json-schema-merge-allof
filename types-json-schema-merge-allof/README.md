# Installation
> `npm install --save @types/json-schema-merge-allof`

# Summary
This package contains type definitions for json-schema-merge-allof (https://github.com/mokkabonna/json-schema-merge-allof#readme).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/json-schema-merge-allof.

### Additional Details
 * Last updated: Thu, 08 Jul 2021 16:22:51 GMT
 * Dependencies: [@types/json-schema](https://npmjs.com/package/@types/json-schema)
 * Global values: none

# Credits
These definitions were written by [Emily Marigold Klassen](https://github.com/forivall).
