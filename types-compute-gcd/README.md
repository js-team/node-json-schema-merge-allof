# Installation
> `npm install --save @types/compute-gcd`

# Summary
This package contains type definitions for compute-gcd (https://github.com/compute-io/gcd).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/compute-gcd.

### Additional Details
 * Last updated: Wed, 12 May 2021 00:31:26 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by [Sean S. LeBlanc](https://github.com/seleb).
